<?php

namespace App\Recipes;

use PiPHP\GPIO\Pin\InputPinInterface;
use PiPHP\GPIO\Pin\OutputPinInterface;

abstract class Recipe
{
    const WATER_PUMP_PIN = 27;

    public function add($mixer)
    {
        $gpio = $mixer->gpio();

        $input = $gpio->getInputPin(static::INPUT_PIN);
        $input->setEdge(InputPinInterface::EDGE_FALLING);

        $water = $gpio->getOutputPin(static::WATER_PUMP_PIN);
        $juice = $gpio->getOutputPin(static::JUICE_PUMP_PIN);

        $mixer->watcher()->register($input, function ($pin, $value) use ($mixer, $water, $juice) {
            if ($mixer->mixing()) {
                return true;
            }

            $mixer->mixing(true);

            $this->mix($water, $juice, $value);

            $mixer->mixing(false);

            return true;
        });
    }

    public function wait($seconds)
    {
        sleep($seconds);

        return $this;
    }

    public function on($pump)
    {
        $pump->setValue(OutputPinInterface::VALUE_HIGH);

        return $this;
    }

    public function off($pump)
    {
        $pump->setValue(OutputPinInterface::VALUE_LOW);

        return $this;
    }

    abstract public function mix($water, $juice, $value = null);
}
