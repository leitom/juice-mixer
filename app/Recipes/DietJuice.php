<?php

namespace App\Recipes;

class DietJuice extends Recipe
{
    const INPUT_PIN = 13;
    const JUICE_PUMP_PIN = 17;

    public function mix($water, $juice, $value = null)
    {
        $this
            ->on($water)
            ->on($juice)
            ->wait(3)
            ->off($juice)
            ->wait(5)
            ->off($water);
    }
}
