<?php

namespace App\Recipes;

class RegularJuice extends Recipe
{
    const INPUT_PIN = 5;
    const JUICE_PUMP_PIN = 22;

    public function mix($water, $juice, $value = null)
    {
        $this
            ->on($water)
            ->on($juice)
            ->wait(3)
            ->off($juice)
            ->wait(5)
            ->off($water);
    }
}
