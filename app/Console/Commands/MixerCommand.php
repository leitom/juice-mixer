<?php

namespace App\Console\Commands;

use Facades\App\Mixer;
use Illuminate\Console\Command;

class MixerCommand extends Command
{
    protected $signature = 'mixer:serve';
    protected $description = 'Start the mixer daemon.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Mixer::serve();
    }
}
