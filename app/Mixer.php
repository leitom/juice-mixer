<?php

namespace App;

use PiPHP\GPIO\GPIO;
use PiPHP\GPIO\Pin\InputPinInterface;

class Mixer
{
    const SHUTDOWN_PIN = 3;

    protected $gpio;
    protected $watcher;
    protected $mixing = false;
    protected $recipes = [Recipes\DietJuice::class, Recipes\RegularJuice::class,];

    public function __construct()
    {
        $this->gpio = new GPIO();
        $this->watcher = $this->gpio->createWatcher();

        $this->addRecipies()
             ->registerShutdown();
    }

    public function serve()
    {
        while($this->watcher->watch(5000));
    }

    public function mixing($mixing = null)
    {
        if (! is_null($mixing)) {
            $this->mixing = $mixing;
        }

        return $this->mixing;
    }

    public function gpio()
    {
        return $this->gpio;
    }

    public function watcher()
    {
        return $this->watcher;
    }

    protected function addRecipies()
    {
        foreach ($this->recipes as $recipe) {
            app($recipe)->add($this);
        }

        return $this;
    }

    protected function registerShutdown()
    {
        $input = $this->gpio->getInputPin(static::SHUTDOWN_PIN);
        $input->setEdge(InputPinInterface::EDGE_FALLING);

        $this->watcher->register($input, function ($pin, $value) {
            exec('sudo shutdown -h now > /dev/null &');
        });

        return $this;
    }
}
